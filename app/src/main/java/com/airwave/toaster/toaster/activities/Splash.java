package com.airwave.toaster.toaster.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.airwave.toaster.toaster.R;
import com.airwave.toaster.toaster.consts.Constants;
import com.airwave.toaster.toaster.utils.Utils;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.User;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class Splash extends AppCompatActivity {

    private static final int FINISH_REQUEST_CODE = 100;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));

        context = this;

        TwitterSession session =
                Twitter.getSessionManager().getActiveSession();

        if (session == null) {
            Intent intent = new Intent(this, LogIn.class);
            startActivityForResult(intent,FINISH_REQUEST_CODE);
        } else {
            Utils.showErrorLog("TAG", "Reached Here");

            Call<User> userCall = Twitter.getApiClient(session).getAccountService()
                    .verifyCredentials(true, false);

            userCall.enqueue(new Callback<User>() {
                @Override
                public void success(Result<User> result) {

                    Utils.showErrorLog("TAG", "Result is : " + new Gson().toJson(result));

                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.putExtra("UserData", new Gson().toJson(result));
                    startActivityForResult(intent,FINISH_REQUEST_CODE);
                }

                @Override
                public void failure(TwitterException exception) {

                    Toast.makeText(context, "Oops! Something went wrong!", Toast.LENGTH_LONG).show();
                    Utils.showErrorLog("TAG", exception.getMessage());
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==FINISH_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                finish();
            }
        }
    }
}
