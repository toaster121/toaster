package com.airwave.toaster.toaster.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airwave.toaster.toaster.R;
import com.airwave.toaster.toaster.consts.Constants;
import com.airwave.toaster.toaster.utils.Utils;
import com.airwave.toaster.toaster.volley.VolleySingleton;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.SearchService;
import com.twitter.sdk.android.core.services.params.Geocode;
import com.twitter.sdk.android.tweetui.CompactTweetView;
import com.twitter.sdk.android.tweetui.TweetUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnCameraMoveListener  {

    private static final float CAMERA_ZOOM = 5;
    private static final int TWEET_RADIUS = 10;
    final private int REQUEST_CODE_LOCATION_PERMISSION_1 = 100;
    final private int REQUEST_CODE_LOCATION_PERMISSION_2 = 200;
    private static final int FINISH_REQUEST_CODE = 100;


    static String TAG = "TAG";

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    Activity activity;
    Geocode geocode;
    int tweetCount;
    LatLng[] tweetLatLng;
    Geocoder geocoder;
    FrameLayout userTweet;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle drawerToggle;
    NavigationView navigationView;
    boolean loadTweets = true;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton tweetListFab = (FloatingActionButton) findViewById(R.id.tweetListFab);
        userTweet = (FrameLayout) findViewById(R.id.userTweet);
        navigationView = (NavigationView) findViewById(R.id.navigationView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);

        try {
            initDrawer();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        activity = this;
        activity.setResult(RESULT_OK);
        geocoder = new Geocoder(this);

        //map init
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //googleApiClient init
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        tweetListFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, TweetList.class);
                startActivity(intent);
            }
        });
    }

    private void getNearbyTweeets() {

        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        SearchService searchService = twitterApiClient.getSearchService();

        Call<Search> call = searchService.tweets(null, geocode, "en", null, "recent", 100, null, null, null, null);
        call.enqueue(new Callback<Search>() {
            @Override
            public void success(final Result<Search> result) {

                tweetLatLng = new LatLng[result.data.tweets.size()];
                Utils.showErrorLog("TAG", "Result size is " + result.data.tweets.size());

                tweetCount = result.data.tweets.size();
                for (int i = 0; i < result.data.tweets.size(); i++) {

                    //Ignoring empty locations
                    if (!result.data.tweets.get(i).user.location.isEmpty()) {
                        String location = result.data.tweets.get(i).user.location;

                        //Replacing " " with %20 for ease in making URL for geocoding
                        if (result.data.tweets.get(i).user.location.contains(" ")) {
                            location = location.replace(" ", "%20");
                        }

                        String url = Constants.GEOCODING1 + location + Constants.GEOCODING2;

                        final int finalI = i;

                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url
                                , null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    double lng = response.getJSONArray("results").getJSONObject(0)
                                            .getJSONObject("geometry").getJSONObject("location")
                                            .getDouble("lng");

                                    double lat = response.getJSONArray("results").getJSONObject(0)
                                            .getJSONObject("geometry").getJSONObject("location")
                                            .getDouble("lat");

                                    tweetLatLng[finalI] = new LatLng(lat, lng);

                                    if (tweetLatLng[finalI] != null) {

                                        //adding marker to the map and setting its tag as the tweet id
                                        mMap.addMarker(new MarkerOptions().position(tweetLatLng[finalI])
                                                .title("Tweet " + finalI)).setTag(result.data.tweets.get(finalI).id);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {


                            }
                        });
                        VolleySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
                    }
                }

            }

            public void failure(TwitterException exception) {
                Utils.showErrorLog("TAG", "Error : " + exception.getMessage());
            }
        });

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraMove() {

        if (userTweet.getVisibility() == View.VISIBLE) {
            userTweet.removeAllViews();
            userTweet.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        for (int i = 0; i < tweetCount; i++) {

            if (marker.getTitle().equals("Tweet " + i)) {

                final List<Long> tweetIds = Arrays.asList((Long) marker.getTag());

                for (Long longVar : tweetIds)
                    TweetUtils.loadTweet(longVar, new Callback<Tweet>() {
                        @Override
                        public void success(Result<Tweet> result) {

                            CompactTweetView tweetView = new CompactTweetView(activity, result.data,
                                    R.style.tw__TweetDarkWithActionsStyle);

                            userTweet.addView(tweetView);

                            if (userTweet.getVisibility() == View.GONE) {
                                userTweet.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void failure(TwitterException exception) {

                            Utils.showErrorLog(TAG,"Error : "+ exception.getMessage());
                            Toast.makeText(activity,"Oops! Somthing went Wrong!",Toast.LENGTH_LONG).show();
                        }
                    });
            }
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setOnCameraMoveListener(this);

        getCurrentLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {

            case REQUEST_CODE_LOCATION_PERMISSION_2:

                if (grantResults.length > 0 &&grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    getCurrentLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Location Access Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    private void getCurrentLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_LOCATION_PERMISSION_2);
            }
            return;
        }

        mMap.setMyLocationEnabled(true);
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {

            double longitude = location.getLongitude();
            double latitude = location.getLatitude();

            LatLng currentLatLng = new LatLng(latitude, longitude);
            //mMap.addMarker(new MarkerOptions().position(currentLatLng).draggable(false).title("Current Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(CAMERA_ZOOM));

            geocode = new Geocode(latitude, longitude, TWEET_RADIUS, Geocode.Distance.KILOMETERS);

            if (loadTweets) {

                getNearbyTweeets();

                loadTweets = false;
            }

        }

    }
    private void initDrawer() throws JSONException {

        final String userData=getIntent().getExtras().getString("UserData");
        Utils.showErrorLog(TAG,"Result : "+ userData);

        JSONObject userDataJson= new JSONObject(userData);

        View header=navigationView.getHeaderView(0);

        ImageView userImage= (ImageView) header.findViewById(R.id.userImage);
        TextView userName = (TextView) header.findViewById(R.id.userName);

        Utils.showErrorLog(TAG,"Image URl = " + userDataJson.getJSONObject("data").getString("profile_image_url"));
        Picasso.with(this).load(userDataJson.getJSONObject("data").getString("profile_image_url")).into(userImage);
        userName.setText(userDataJson.getJSONObject("data").getString("name").toUpperCase());

        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity,UserProfile.class);
                intent.putExtra("UserData",userData);
                startActivity(intent);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.open_drawer, R.string.close_drawer) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }
        };
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId())
                {
                    case R.id.navItem1:
                    break;
                    case R.id.navItem2:
                        Toast.makeText(activity, "Coming Soon!", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.navItem3:
                        Toast.makeText(activity, "Congrats! You just clicked a completely useless button!", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.navItem4:
                        Toast.makeText(activity, "Congrats! You just clicked a completely useless button!", Toast.LENGTH_LONG).show();
                        break;
                    case R.id.navItem5:
                    {
                        Twitter.getSessionManager().clearActiveSession();
                        Twitter.logOut();
                        Intent intent = new Intent(activity,LogIn.class);
                        startActivityForResult(intent,FINISH_REQUEST_CODE);
                        break;
                    }
                }

                drawerLayout.closeDrawer(GravityCompat.START);
                return false;


            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        drawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        }
        else if(userTweet.getVisibility()==View.VISIBLE)
        {
            userTweet.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode==FINISH_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                finish();
            }
        }
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_outlets, menu);
//
//            menu.findItem(R.id.menu_item_refresh).setIcon(R.drawable.refresh_arrow);
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                onBackPressed();
//                break;
//
//            case R.id.menu_item_refresh:
//
//                getNearbyTweeets();
//                Toast.makeText(activity,"Refreshing..",Toast.LENGTH_LONG).show();
//                return true;
//        }
//
//
//        return super.onOptionsItemSelected(item);
//    }
}
