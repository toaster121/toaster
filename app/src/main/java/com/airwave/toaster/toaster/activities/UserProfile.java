package com.airwave.toaster.toaster.activities;

import android.app.ListActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import com.airwave.toaster.toaster.R;
import com.airwave.toaster.toaster.utils.Utils;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

import org.json.JSONException;
import org.json.JSONObject;

public class UserProfile extends ListActivity {

    JSONObject jsonObject;
    UserTimeline userTimeline;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_user_profile);


        try {

            jsonObject = new JSONObject(getIntent().getExtras().getString("UserData"));
            userTimeline = new UserTimeline.Builder().userId(jsonObject.getJSONObject("data").getLong("id")).build();
            getActionBar().setTitle("@" + jsonObject.getJSONObject("data").getString("screen_name"));

            final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(this)
                    .setTimeline(userTimeline)
                    .build();
            setListAdapter(adapter);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
