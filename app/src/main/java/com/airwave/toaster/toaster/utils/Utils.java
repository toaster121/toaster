package com.airwave.toaster.toaster.utils;

import android.content.Context;
import android.util.Log;

/**
 * Created by Baba on 27-02-2017.
 */

public class Utils {

    static boolean showLog = true;

    public static void showErrorLog(String tag, String message)

    {
        if (showLog) {
            Log.d(tag, message);
        }
    }
}
