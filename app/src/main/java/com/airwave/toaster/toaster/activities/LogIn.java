package com.airwave.toaster.toaster.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.airwave.toaster.toaster.R;
import com.airwave.toaster.toaster.consts.Constants;
import com.google.gson.Gson;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

public class LogIn extends AppCompatActivity {

    private TwitterLoginButton twitterLoginButton;
    Context context = this;
    Activity activity;
    private static final int FINISH_REQUEST_CODE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        activity=this;
        activity.setResult(RESULT_OK);

        twitterLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);

        twitterLoginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {

                Call<User> userCall = Twitter.getApiClient(Twitter.getSessionManager().getActiveSession()).getAccountService()
                        .verifyCredentials(true, false);

                userCall.enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {

                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.putExtra("UserData", new Gson().toJson(result));
                        startActivityForResult(intent,FINISH_REQUEST_CODE);
                    }

                    @Override
                    public void failure(TwitterException exception) {

                        Toast.makeText(context,"Oops! Something went wrong!",Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {

                Toast.makeText(context, "Oops! Something went wrong!", Toast.LENGTH_LONG).show();
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        twitterLoginButton.onActivityResult(requestCode, resultCode, data);

        if(requestCode==FINISH_REQUEST_CODE)
        {
            if(resultCode== RESULT_OK)
            {
                finish();
            }
        }
    }
}
